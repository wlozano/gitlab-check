#!/usr/bin/python3

import argparse
import gitlab
import logging
import sys
from config import config

RETRY_NONE = 0
RETRY_PIPELINE = 1
RETRY_JOBS = 2
RETRY_RETRIGGER = 3

VERBOSE_NORMAL = 1
VERBOSE_FULL = 2

class GitlabCheck():
    def __init__(self, config, args):
        if args.url:
            self.gitlab = gitlab.Gitlab(args.url, args.token)
        elif args.instance:
            self.gitlab = gitlab.Gitlab.from_config(config['instance'])
        elif config['url']:
            self.gitlab = gitlab.Gitlab(config['url'], config['token'])
        elif config['instance']:
            self.gitlab = gitlab.Gitlab.from_config(config['instance'])
        else:
            self.gitlab = gitlab.Gitlab.from_config()

        self.gitlab.auth()

        self.all = args.all
        self.config = args.config
        self.debug = args.debug
        self.expanded_projects = []
        self.fetch_all = args.fetch_all
        self.projects = None
        self.ref = args.ref or config['ref']
        self.retry = args.retry
        self.status = args.status or config['status']
        self.verbose = args.verbose

    def expand_projects(self, projects):
        projects_to_check = set(projects)

        for project in projects_to_check:
            if self.debug: print(f'Expanding {project} to:')

            if project.endswith("/*"):
                project_path = project.replace('/*', '')
                project_group = self.gitlab.groups.get(project_path, lazy=True)
                list_subprojects = project_group.projects.list(get_all=True)

                for subproject in list_subprojects:
                    subproject_id = self.gitlab.projects.get(subproject.id)
                    subproject_branches = subproject_id.branches.list(get_all=True)
                    subproject_branches = [project.name for project in subproject_branches]

                    if set(self.ref) & set(subproject_branches):
                        self.expanded_projects.append(subproject_id.path_with_namespace)
                        if self.debug: print(f'  {subproject_id.path_with_namespace}')
            else:
                self.expanded_projects.append(project)
                if self.debug: print(f'  {project}')

    def fetch_pipelines(self, project_name):
        project = self.gitlab.projects.get(project_name)
        pipelines = project.pipelines.list(get_all=self.fetch_all)

        return pipelines

    def fetch_branches(self, project_name):
        project = self.gitlab.projects.get(project_name)
        branches = project.branches.list(get_all=True)

        return branches

    def existing_branches(self, project_name, pipelines):
        branches = self.fetch_branches(project_name)
        branch_names = [x.name for x in branches]
        pipelines = [x for x in pipelines if x.ref in branch_names]

        return pipelines

    def newest_pipelines(self, pipelines):
        pipelines_cache = {}
        for p in pipelines:
            if p.ref in pipelines_cache.keys():
                if pipelines_cache[p.ref].id < p.id:
                    pipelines_cache[p.ref] = p
            else:
                pipelines_cache[p.ref] = p

        return pipelines_cache.values()

    def filter_pipelines(self, pipelines):
        if self.ref:
            pipelines = [p for p in pipelines if p.ref in self.ref]
        if self.status and self.status != '-':
            pipelines = [p for p in pipelines if p.status == self.status]

        return pipelines

    def print_pipelines(self, project_name, pipelines):
        for p in pipelines:
            if self.verbose == VERBOSE_NORMAL:
                print(project_name, p.id, p.ref, p.created_at, p.status)
            elif self.verbose == VERBOSE_FULL:
                print(project_name, p.id, p.ref, p.created_at, p.status, p.web_url)
            else:
                print(p)

    def retry_pipelines(self, project_name, pipelines):
        for p in pipelines:
            print(f'Retrying pipelines for {project_name} {p.ref}')
            p.retry()

    def retry_jobs(self, project_name, pipelines):
        def is_in_cache(jobs_cache, job):
            for j in jobs_cache:
                if  j.name  == job.name and j.id > job.id:
                    return True
                return False

        for p in pipelines:
            project = self.gitlab.projects.get(project_name)
            jobs = project.jobs.list(get_all=self.fetch_all)
            jobs_cache = []
            for j in jobs:
                if j.ref != p.ref:
                    continue
                if is_in_cache(jobs_cache, j):
                    continue
                jobs_cache.append(j)
                if j.status == 'failed':
                    print(f'Retrying job {j.name} {j.ref}')
                    j.retry()

    def retrigger_pipeline(self, project_name, pipelines):
        for p in pipelines:
            project = self.gitlab.projects.get(project_name)
            pipeline = project.pipelines.create({'ref': p.ref})
            print(f'Pipeline {pipeline}')

    def check_pipelines(self):
        pipeline_count = 0
        for p in self.expanded_projects:
            pipelines = self.fetch_pipelines(p)
            if not self.all:
                pipelines = self.existing_branches(p, pipelines)
                pipelines = self.newest_pipelines(pipelines)
            pipelines = self.filter_pipelines(pipelines)
            pipeline_count += len(pipelines)
            self.print_pipelines(p, pipelines)
            if self.retry == RETRY_PIPELINE:
                self.retry_pipelines(p, pipelines)
            if self.retry == RETRY_JOBS:
                self.retry_jobs(p, pipelines)
            elif self.retry == RETRY_RETRIGGER:
                self.retrigger_pipeline(p, pipelines)

        return pipeline_count

if __name__ == '__main__':
    logging.basicConfig(level = logging.INFO)

    action_choices = [
        'check-pipelines',
    ]

    available_actions = ' '.join(action_choices)

    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--all", action="store_true", help="not only newest objects")
    parser.add_argument("-c", "--config", default="config.yaml", help="configuration file")
    parser.add_argument("-d", "--debug", action="store_true", help="print debug information")
    parser.add_argument("-e", "--error", action="store_true", help="trigger error in case of output")
    parser.add_argument("-f", "--fetch_all", action="store_true", help="fetch all objects")
    parser.add_argument("-i", "--instance", help="Gitlab instance")
    parser.add_argument("-r", "--ref", action='append', help="filter by ref")
    parser.add_argument("-s", "--status", default=None, help="filter by status")
    parser.add_argument("-t", "--token", default="", help="Gitlab token")
    parser.add_argument("-u", "--url", help="Gitlab URL")
    parser.add_argument('-v', '--verbose', type=int, default=VERBOSE_NORMAL, help='verbose')
    parser.add_argument("-x", "--retry", type=int, default=RETRY_NONE, help="retry pipeline: 0 none, 1 pipeline, 2 jobs, 3 retrigger")
    parser.add_argument('action', choices=action_choices, help="action to be executed, available actions: " + available_actions, metavar='action')

    args = parser.parse_args()

    config.load_config(args)

    gitlab_check = GitlabCheck(config, args)

    gitlab_check.expand_projects(config['projects'])

    if args.action == 'check-pipelines':
        pipeline_count = gitlab_check.check_pipelines()
        if args.error and pipeline_count:
            print(f'There are {pipeline_count} pipelines with failure')
            sys.exit(1)
    else:
        print('Invalid action')
